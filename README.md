# Hacking Club CTF

[![unlicense](https://img.shields.io/badge/un-license-brightgreen.svg)](http://unlicense.org "The Unlicense") [![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme "RichardLitt/standard-readme")

> This is the code for a DJANGO Based CTF Platform I used to host an Introductory CTF for my [Hacking Club](https://mjkranch.com/2019/01/hacking_club/) .

This code was used to host a CTF as part of a self-started [hacking club](https://mjkranch.com/2019/01/hacking_club/) I created for my peers while attending CGSC. I under-estimated the amount of work it would take to start a club from scratch a a side-project so this code is not nearly as polished as I want; however, it worked and the students had a great time. You can access the slides that correspond to this IntroCTF [here](https://mjkranch.com/meetings/mtg1.html#/ctf).

This is a fork of [MINICTF](https://github.com/DivyanshuSahu/miniCTF) - A platform build in Django for hosting CTF events. Major changes include support for hosting via google cloud, additional challenge categories, and several security/account improvements include captcha support, email account validation, and password reset securely via email [details in security](#security). I also had to do a ton of core code re-writing including code-reuse (like [./templates/challenges.html](./templates/challenges.html)) and compare to the original), adding Foreign-keys (like [challenges/models.py](./challenges/models.py)), incorporating the AbstractUser model (like [/accounts/models.py](./accounts/models.py)), and input validation.

### WARNING:

Again, this code was never intended to originally be released, but I decided to recently to increase my code portfolio. I am sure there are still quite a bit of quirks that need to be worked out in the code so use at your own risk.

## Table of Contents

- [Background](#background)
- [Security](#security)
- [Live Demo](#live-demo)
- [Install Instructions](#install)
- [Contribute](#contribute)
- [License](#license)

## Background

I started a hacking club  to teach some of my peers about cybersecurity through learning how to hack. I wanted to create a simple CTF for my lessons. I was interested in learning Django as well so decided to build upon on an existing Django CTF platform.

## Security

I implemented [captcha](./templates/_recaptcha.html) on [user account registration](./accounts/templates/accounts/team_register.html). I also added support for all the [traditional account registration functions](./templates/registration) like email validation and email based password reset. I had to make changes in tons of functions including major changes to [accounts/views](./accounts/views.py) (look at `TeamCreateView()->post()` for example), and this required use of a [token generator](./accounts/tokens.py).

Also, I added files to help product keys through use of env_variables. Both [set_env_vars_git.sh](./set_env_vars_git.sh) and [app_for_git.yaml](./app_for_git.yaml) are examples of how to handle the numerous keys (Django, captcha, gcloud, etc.) without pushing to Gitlab.

## Live demo

~~ https://ctf.mjkranch.com/ ~~ - this site is no longer operational

The IntroCTF Challenge Page

<img src="docs/imgs/my_challenges.png" width="100%">

The IntroCTF Scoreboard Example

<img src="docs/imgs/my_scoreboard.png" width="100%">


## Install

### Requirements

```
python 3.5.x
django 2.0
```

### Install django

```sh
$ sudo pip3 install django
```

### Generate a secret KEY

from [here](https://foxrow.com/generating-django-secret-keys)
`python
>>> import random
>>> ''.join(random.SystemRandom().choice('abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)') for i in range(50))`

Save the resulting secret value as the environment variable KEY
`$ KEY='<secret_value>'`
Test to see if your key was set
`$ echo KEY`
 [Make sure you also set this value in your CI/CD](https://docs.gitlab.com/ee/ci/variables/#variables)

### Install introCTF

```sh
$ git clone https://gitlab.com/archang31/ctf.git
$ python manage.py makemigrations accounts challenges forums
$ python manage.py migrate
$ python manage.py runserver
```

### Configure G-cloud

https://cloud.google.com/python/django/appengine

Use the start SDK and cloud proxy install
Created a SQL instance instead of postgres - https://cloud.google.com/sql/docs/mysql/create-instance

`gcloud sql instances describe django-ctf-sql`
instance name - `django-ctf:us-central1:django-ctf-sql`

set creds (do not push to GitLab):
`export GOOGLE_APPLICATION_CREDENTIALS='/users/archang31/.google_sdk/django_ctf_cloud_sql_service_acct.json'`

`cloud_sql_proxy -instances="django-ctf:us-central1:django-ctf-sql"=tcp:3306`

Create database and USER.

```
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```

you will get a mysql error, need to simply install mysql. Also, add `Mysqlclient==1.3.13` to requirements.txt
I also `pip3 install mysqlclient` but I do not think I needed to do this action.

```
brew install mysql
python manage.py makemigrations
python manage.py makemigrations polls
python manage.py makemigrations accounts challenges forums
python manage.py migrate

python manage.py createsuperuser

python manage.py collectstatic

gcloud app deploy
```

SQL instances
https://console.cloud.google.com/sql/instances?project=django-ctf&folder&organizationId

Make sure to go to the versions page to delete old app versions
https://console.cloud.google.com/appengine/versions?project=django-ctf&folder&organizationId&serviceId=default&versionssize=50

Then register or create superuser and add challenges.


## Contribute

> Contributors to this project are expected to adhere to our [Code of Conduct](CODE_OF_CONDUCT.md "Code of Conduct").

I welcome [issues](docs/issue_template.md "Issue template"), but I prefer [pull requests](docs/pull_request_template.md "Pull request template")! See the [contribution guidelines](contributing.md "Contributing") for more information.


## License

This code is [set free](LICENSE).
