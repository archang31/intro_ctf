from django import forms
from . import models

class AddChallengeForm(forms.ModelForm) :
	name = forms.CharField(max_length=250, label="Challenge Name *", widget=forms.TextInput(attrs={'placeholder':'Challenge Name','class':'form-control'}))
	category = forms.CharField(widget=forms.Select(choices=models.Challenges.category_list, attrs={'class':'form-control'}), label="Challenge Category *")
	description = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'placeholder':'Challenge Description', 'class':'form-control','required':'false'}), label="Challenge Description *")
	hint1 = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'placeholder':'Hint1 (if one, use in order):', 'class':'form-control','required':'false'}), label="Hint1", required=False)
	hint2 = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'placeholder':'Hint2 (if a second, use in order):', 'class':'form-control','required':'false'}), label="Hint2", required=False)
	hint3 = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'placeholder':'Hint3 (if a third, use in order):', 'class':'form-control','required':'false'}), label="Hint3", required=False)
	points = forms.IntegerField(widget=forms.NumberInput(attrs={'class':'form-control'}), label="Challenge Points *")
	file = forms.FileField(label="Challenge Files (if any)", required=False)
	flag = forms.CharField(max_length=253, label="Challenge Flag *", widget=forms.TextInput(attrs={'placeholder':'Challenge Flag','class':'form-control'}))
	author = forms.CharField(max_length=250, label="Challenge Author *", widget=forms.TextInput(attrs={'placeholder':'Challenge Author','class':'form-control'}))
	#solved_by = forms.CharField(max_length=250)

	class Meta :
		model = models.Challenges
		fields = ["name","category","description","hint1","hint2","hint3","points","file","flag","author"]
