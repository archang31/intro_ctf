from django.db import models
from django.utils import timezone
import hashlib

# Create your models here.

def get_upload_path(instance, filename):
	return instance.category+'/challenges_{0}/{1}'.format(hashlib.md5(instance.name.encode('utf-8')).hexdigest(), filename)

class Challenges(models.Model):
	category_list = [
		('Cryptography','Cryptography'),
		('Forensics','Forensics'),
		('Pwning','Pwning'),
		('Reverse Engineering','Reverse Engineering'),
		('Promo Posters','Promo Posters'),
		('Web','Web')
	]

	name = models.CharField(max_length=250, unique=True)
	challenge_id = models.CharField(max_length=251, primary_key=True)
	category = models.CharField(max_length=100)
	description = models.CharField(max_length=255, blank=True, default="")
	hint1 = models.CharField(max_length=255, blank=True, default="")
	hint2 = models.CharField(max_length=255, blank=True, default="")
	hint3 = models.CharField(max_length=255, blank=True, default="")
	points = models.IntegerField()
	file = models.FileField(null=True, blank=True, upload_to=get_upload_path)
	flag = models.CharField(max_length=252)
	author = models.CharField(max_length=250)
	first_blood = models.ForeignKey('accounts.Team', on_delete=models.SET_DEFAULT, null=True, default=None)
	first_blood_time = models.DateTimeField(default=None, null=True)
	solves = models.IntegerField(default=0)


class ChallengesSolvedBy(models.Model):
	challenge =  models.ForeignKey('Challenges', on_delete=models.CASCADE)
	user_name = models.ForeignKey('accounts.Team', on_delete=models.CASCADE)
	attempts = models.IntegerField(default=1)
	solved_time = models.DateTimeField(auto_now_add=True)

class ChallengesSolveAttempts(models.Model):
	challenge=  models.ForeignKey('Challenges', on_delete=models.CASCADE)
	user_name = models.ForeignKey('accounts.Team', on_delete=models.CASCADE)
	attempt_time = models.DateTimeField(auto_now_add=True)
	flag = models.CharField(max_length=252, null=True)
	result = models.CharField(max_length=100, default="")
