from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.utils import timezone
from . import forms
from . import models
from accounts import models as accounts_models

# Create your views here.

class PassInsideView() :
	view_dict = dict()
	name = ''
	challenge_id = ''
	category = ''
	description = ''
	hint1 = ''
	hint2 = ''
	hint3 = ''
	points = ''
	file = ''
	flag = ''
	author = ''
	solves = ''


	def __init__(self, challenge) :
		self.name = challenge.name
		self.challenge_id = challenge.challenge_id
		self.category = challenge.category
		self.description = challenge.description
		self.points = challenge.points
		self.file = challenge.file
		self.flag = challenge.flag
		self.author = challenge.author
		self.hint1 = challenge.hint1
		self.hint2 = challenge.hint2
		self.hint3 = challenge.hint3
		self.first_blood = challenge.first_blood
		self.first_blood_time = challenge.first_blood_time
		self.solves = challenge.solves

def assignID(a) :
	return a.lower().replace(' ','_')

@login_required(login_url="/team/login/")
def index(request) :
	challenge = models.Challenges.objects.order_by("points")
	challenge_info_stego_object = []
	challenge_info_for_object = []
	challenge_info_re_object = []
	challenge_info_pwn_object = []
	challenge_info_web_object = []
	challenge_info_crypto_object = []
	for c in challenge :
		if c.category == 'Promo Posters' :
			s = PassInsideView(c)
			challenge_info_stego_object.append(s)
		elif c.category == 'Reverse Engineering' :
			re = PassInsideView(c)
			challenge_info_re_object.append(re)
		elif c.category == 'Forensics' :
			f = PassInsideView(c)
			challenge_info_for_object.append(f)
		elif c.category == 'Pwning' :
			p = PassInsideView(c)
			challenge_info_pwn_object.append(p)
		elif c.category == 'Web' :
			w = PassInsideView(c)
			challenge_info_web_object.append(w)
		elif c.category == 'Cryptography' :
			cy = PassInsideView(c)
			challenge_info_crypto_object.append(cy)

	solved_challenges_by_user_id = []
	solved_challenges = dict()
	try :
		fc = models.ChallengesSolvedBy.objects.filter(user_name=request.user)
		for f in fc :
			solved_challenges_by_user_id.append(f.challenge.challenge_id)
			solved_challenges[f.challenge.challenge_id] = f
	except :
		pass

	return render(request, 'challenges.html',{
		'data_stego':challenge_info_stego_object,
		'data_for':challenge_info_for_object,
		'data_re':challenge_info_re_object,
		'data_pwn':challenge_info_pwn_object,
		'data_web':challenge_info_web_object,
		'data_crypto':challenge_info_crypto_object,
		'user_solved_by_id':solved_challenges_by_user_id,
		'user_solved':solved_challenges})

def handle_correct_submission(request, challenge, challenge_solved):
	#challenge_points = challenge.points
	challenge_solved.save()
	# update team points and solves
	team = accounts_models.Team.objects.get(username=request.user)
	#initial_points = accounts_models.Team.objects.get(username=request.user).points
	#updated_team_points = initial_points + challenge_points
	updated_team_points = team.points + challenge.points
	#updated_team_solves = accounts_models.Team.objects.get(username=request.user).solves + 1
	#accounts_models.Team.objects.filter(username=request.user).update(points=updated_team_points, solves=updated_team_solves)
	team.points=updated_team_points
	#team.solves=updated_team_solves
	team.solves=team.solves + 1
	team.save()
	# update challenge solves
	if challenge.solves == 0:
		challenge.first_blood=team
		challenge.first_blood_time=timezone.now()
	challenge.solves=challenge.solves + 1
	challenge.save()
	return '<div id="flag_correct"><p>CORRECT</p></div>'


@login_required(login_url="/team/login/")
def flagsubmit(request) :
	if request.method == 'POST' :
		flag_submit = ''
		flag_submit_id = ''
		x = ''
		for k in request.POST :
			if k == 'submit' :
				continue
			if k == 'csrfmiddlewaretoken' :
				continue
			else :
				x = k
		flag_submit = request.POST[x]
		flag_submit_id = x[:-5]
	challenge = models.Challenges.objects.get(challenge_id=flag_submit_id)
	flag = challenge.flag
	challenge_points = challenge.points
	if 	request.user.is_superuser :
		# DO not log superuser test submissions
		if flag == flag_submit:
			response = '<div id="flag_already"><p>CORRECT, but not added to scoreboard since superuser</p></div>'
		else:
			response = '<div id="flag_incorrect"><p>INCORRECT, but not logged since superuser</p></div>'
	else:
		if flag == flag_submit:
			submission_result="CORRECT"
			# create ChallangeSolvedBy message
			attempts = models.ChallengesSolveAttempts.objects.filter(challenge=challenge, user_name=request.user).count() + 1
			challenge_solved = models.ChallengesSolvedBy(challenge=challenge, user_name=request.user, attempts=attempts)
			try :
				fc = models.ChallengesSolvedBy.objects.filter(user_name=request.user)
				obs = []
				for k in fc :
					# remember, k first challenge_id is an object, second is a field within object
					obs.append(k.challenge.challenge_id)
				if flag_submit_id in obs :
					response = '<div id="flag_already"><p>ALREADY SUBMITTED</p></div>'
					submission_result="ALREADY SUBMITTED"
				else :
					response = handle_correct_submission(request, challenge, challenge_solved)
			except :
				# not sure when you should end up here.
				response = handle_correct_submission(request, challenge, challenge_solved)
		else :
			submission_result="INCORRECT"
			response = '<div id="flag_incorrect"><p>INCORRECT</p></div>'
		# add an additional solve attempt (regardless of result)
		attempt = models.ChallengesSolveAttempts(challenge=challenge, user_name=request.user, result=submission_result, flag=flag_submit)
		attempt.save()
	return HttpResponse(response)

@login_required(login_url="/team/login/")
def addchallenges(request) :
	if request.user.is_superuser :
		if request.method == 'POST' :
			success = 0
			form = forms.AddChallengeForm(request.POST, request.FILES)
			if form.is_valid() :
				success = 1
				#print(request.FILES)
				if request.FILES :
					i = models.Challenges(file=request.FILES['file'],
						name=request.POST['name'],
						category=request.POST['category'],
						description=request.POST['description'],
						hint1=request.POST['hint1'],
						hint2=request.POST['hint2'],
						hint3=request.POST['hint3'],
						points=request.POST['points'],
						challenge_id=assignID(request.POST['name']),
						flag=request.POST['flag'],
						author=request.POST['author'])
					i.save()
				else :
					i = models.Challenges(
						name=request.POST['name'],
						category=request.POST['category'],
						description=request.POST['description'],
						hint1=request.POST['hint1'],
						hint2=request.POST['hint2'],
						hint3=request.POST['hint3'],
						points=request.POST['points'],
						challenge_id=assignID(request.POST['name']),
						flag=request.POST['flag'],
						author=request.POST['author'])
					i.save()
				return render(request, 'addchallenges.html', {'form':form,'success':success})
		else :
			form = forms.AddChallengeForm()

		return render(request, 'addchallenges.html', {'form':form})

	else :
		return redirect("/")
