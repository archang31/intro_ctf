from django.template.defaulttags import register
from django.utils.formats import date_format


# https://docs.djangoproject.com/en/dev/howto/custom-template-tags/#writing-custom-template-tags




@register.simple_tag
def get_field_from_key_of_dict(dict, key, field):
    target_object = dict[key]
    # https://stackoverflow.com/questions/6305061/get-an-object-attribute
    return getattr(target_object, field)

@register.simple_tag
def get_pretty_date(dict, key, field):
    date_obj = get_field_from_key_of_dict(dict, key, field)
    date = date_format(date_obj, format='DATETIME_FORMAT', use_l10n=True)
    return date
