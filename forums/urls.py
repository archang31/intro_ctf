from django.urls import path, re_path
from . import views

urlpatterns = [
	path('', views.index, name="forum_index"),
	re_path('(?P<pk>.+)', views.thread, name="forum_thread")
]
