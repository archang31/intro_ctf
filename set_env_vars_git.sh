#!/usr/bin/env bash
# run with '. set_env_vars.sh' or 'source set_env_vars.sh'




export DJANGO_KEY=''
export DJANGO_DEBUG=true
export DATABASE_USER=''
export DATABASE_PASSWORD=''
export DATABASE_HOST=''
export DATABASE_NAME=''
# GOOGLE DEBUG KEY -> replace with your own
export RECAPTCHA_PUBLIC_KEY='6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
export RECAPTCHA_PRIVATE_KEY='6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'
# GOOGLE REAL KEY
#export RECAPTCHA_PUBLIC_KEY='replace_with_your_own'
#export RECAPTCHA_PRIVATE_KEY='replace_with_your_own'
export EMAIL_HOST=''
export EMAIL_HOST_USER=''
export EMAIL_HOST_PASSWORD=''
export EMAIL_PORT=587

export LOCAL_DATABASE=false
echo 'Done exporting all enviroment variables'
