from django.conf import settings

# https://chriskief.com/2013/09/19/access-django-constants-from-settings-py-in-a-template/
def global_settings(request):
    # return any necessary values
    gs = {
        'RECAPTCHA_PUBLIC_KEY': settings.RECAPTCHA_PUBLIC_KEY
    }

    return gs
