# accounts/urls.py
from django.urls import path, include, re_path, reverse_lazy
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.views.generic.base import RedirectView
from . import views as my_views
from . import views as my_forms

urlpatterns = [
    # Base Team Views
    path('team/', my_views.TeamDetailSelfView.as_view(), name='team'),
    path('team/<int:pk>', my_views.TeamDetailView.as_view(), name='team_detail'),
    path('team/register/', my_views.TeamCreateView.as_view(template_name='accounts/team_register.html'), name='team_register'),
    path('team/update', my_views.TeamUpdateView.as_view(template_name='accounts/team_form.html'), name="team_update"),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', my_views.activate, name='activate'),
    path('team/', include('django.contrib.auth.urls')),
    path('teams/', my_views.TeamListView.as_view(), name='teams'),

    # Score
    path('score/', my_views.TeamScoreSelfDetailView.as_view(), name='score'),
    path('score/<int:pk>', my_views.TeamScoreDetailView.as_view(), name='team_score_detail'),
    path('scoreboard/', my_views.TeamScoreListView.as_view(), name='scoreboard'),

    # Redirects
    path('register/', RedirectView.as_view(url=reverse_lazy('team_register')), name='register'),
    path('update/', RedirectView.as_view(url=reverse_lazy('team_update')), name='update'),
    path('teams/register/', RedirectView.as_view(url=reverse_lazy('team_register'))),
    path('teams/update/', RedirectView.as_view(url=reverse_lazy('team_update'))),
    path('scores/', RedirectView.as_view(url=reverse_lazy('scoreboard'))),



    ### These are the views in django.contrib.auth.urls
    # LoginView = https://docs.djangoproject.com/en/2.1/topics/auth/default/#all-authentication-views
    #path('login/', my_views.CustomLoginView.as_view(template_name='registration/login.html'), name='login'),
    # LogoutView = https://docs.djangoproject.com/en/2.1/topics/auth/default/#django.contrib.auth.views.LogoutView
    #path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),

    #path('password_change/', auth_views.PasswordChangeView.as_view(), name='password_change'),
    #path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),

    #path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    #path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    #path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    #path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
]


"""
https://wsvincent.com/django-user-authentication-tutorial-login-and-logout/

# The views used below are normally mapped in django.contrib.admin.urls.py
# This URLs file is used to provide a reliable view deployment for test purposes.
# It is also provided as a convenience to those who want to deploy these URLs
# elsewhere.

from django.contrib.auth import views
from django.urls import path

urlpatterns = [
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),

    path('password_change/', views.PasswordChangeView.as_view(), name='password_change'),
    path('password_change/done/', views.PasswordChangeDoneView.as_view(), name='password_change_done'),

    path('password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
]
"""
