from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import TeamCreationForm, TeamChangeForm
from .models import Team

class TeamAdmin(UserAdmin):
    add_form = TeamCreationForm
    form = TeamChangeForm
    model = Team
    list_display = ['email', 'username', 'points']

admin.site.register(Team, TeamAdmin)
