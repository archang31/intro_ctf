# accounts/views.py
import urllib
import json

from django.urls import reverse_lazy
from django.views import generic
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.views import LoginView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from .forms import TeamCreationForm, TeamChangeForm
from .models import Team
from challenges.models import ChallengesSolvedBy

# Email imports
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.core.mail import EmailMessage

email_validation = False

# types of form views
# https://docs.djangoproject.com/en/2.1/ref/class-based-views/generic-editing/#django.views.generic.edit.FormView
class TeamCreateView(generic.CreateView):
    form_class = TeamCreationForm
    success_url = reverse_lazy('login')
    template_name = 'accounts/team_register.html' # Default Value is team_create.html

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():

            ''' Begin reCAPTCHA validation '''
            recaptcha_response = request.POST.get('g-recaptcha-response')
            url = 'https://www.google.com/recaptcha/api/siteverify'
            values = {
                'secret': settings.RECAPTCHA_PRIVATE_KEY,
                'response': recaptcha_response
            }
            data = urllib.parse.urlencode(values).encode()
            req =  urllib.request.Request(url, data=data)
            response = urllib.request.urlopen(req)
            result = json.loads(response.read().decode())
            ''' End reCAPTCHA validation '''

            if result['success']:
                if email_validation:
                    user = form.save(commit=False)
                    user.is_active = False
                    user.save()
                    current_site = get_current_site(request)
                    mail_subject = 'Activate your IntroCTF Account'
                    message = render_to_string('accounts/activate_email.html', {
                        'user': user,
                        'domain': current_site.domain,
                        'uid':urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                        'token':account_activation_token.make_token(user),
                    })
                    to_email = form.cleaned_data.get('email')
                    email = EmailMessage(
                                mail_subject, message, to=[to_email]
                    )
                    email.send()
                    messages.success(request, 'Please confirm your email address to complete the registration')
                else:
                    form.save()
                    messages.success(request, 'New user added with success!')
                return redirect(self.success_url)
            else:
                messages.error(request, 'Invalid reCAPTCHA. Please try again.')
                return render(request, 'accounts/team_register.html', {'form': form})
        else:
            handle_form_errors(request, form.errors)
            return render(request, 'accounts/team_register.html', {'form': form})


class TeamUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = Team
    #fields = ['motto', 'website', 'description']
    # no need for a form class if specifying fields
    form_class = TeamChangeForm
    success_url = reverse_lazy('team_update')
    # template_name = 'accounts/team_form.html' # Default Value

    def get_object(self):
        return self.request.user


    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, instance=request.user)
        if form.is_valid() :
            messages.success(request, 'Your profile was successfully updated!')
            return super(generic.UpdateView, self).post(request, *args, **kwargs)
        else :
            handle_form_errors(request, form.errors)
            # returning the form here does not show errors for some reason. I believe
            # its due to password not being present (that is why we do instance=request.user)
            return render(request, 'accounts/team_form.html', {'form': form})


class TeamDetailView(LoginRequiredMixin, generic.DetailView):
    model = Team
    template_name = 'accounts/team_detail.html'
    # Default Value
    # template_name = 'accounts/team_details.html' # Default Value
    #https://stackoverflow.com/questions/41883559/how-to-use-djangos-detailview-or-some-built-in-view-class-for-a-home-page


class TeamDetailSelfView(TeamDetailView):
    model = Team

    #same as team detail but no need to specify PK.
    def get_object(self):
        # sets PK to current logged in user
        return self.request.user


class TeamListView(LoginRequiredMixin, generic.ListView):
    model = Team
    paginate_by = 20
    context_object_name = 'teams'
    ordering = ['username']
    #teams_list = Team.objects.all()
    #query_set = Team.objects.all()
    # template_name = "accounts/team_list.html" # DEFAULT


class TeamScoreDetailView(LoginRequiredMixin, generic.DetailView):
    # https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Generic_views
    model = Team
    template_name = 'accounts/score_detail.html'
    # queryset = Book.objects.filter(title__icontains='war')[:5] # Get 5 books containing the title war

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get the context
        context = super(TeamScoreDetailView, self).get_context_data(**kwargs)
        user_name = context['team']
        solved_challenges = ChallengesSolvedBy.objects.filter(user_name=user_name)
        context['solved_challenges'] = solved_challenges
        return context


class TeamScoreSelfDetailView(TeamScoreDetailView):
    model = Team

    def get_object(self):
        return self.request.user


class TeamScoreListView(LoginRequiredMixin, generic.ListView):
    model = Team
    paginate_by = 20
    context_object_name = 'team_scores'
    template_name = "accounts/score_list.html"
    ordering = ['-points', 'username']

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get the context
        context = super(TeamScoreListView, self).get_context_data(**kwargs)
        #solved_challenges = ChallengesSolvedBy.objects.filter(user_name=user_name)
        #context['solved_challenges'] = solved_challenges
        return context


def handle_form_errors(request, errors):
    for error in errors:
        messages.error(request, error)



# Extra Stuff Not Handled

@login_required(login_url="/team/login/")
def team_view(request) :
	if request.user.is_superuser :
		return HttpResponseRedirect("/teams/")
	team_details = Team.objects.get(username=request.user)
	solved_challenges = ChallengesSolvedBy.objects.filter(user_name=request.user)
	return render(request, 'team/team.html',{'team_details':team_details,'solved_challenges':solved_challenges})


@login_required(login_url="/team/login/")
def every_team(request, pk) :
	requested_team = pk
	try :
		requested_team_details = Team.objects.get(username=requested_team)
		solved_team_challenges = ChallengesSolvedBy.objects.filter(user_name=requested_team)
		return render(request, 'team/team.html', {'team_details':requested_team_details,'solved_challenges':solved_team_challenges})
	except :
		return HttpResponseRedirect("/teams/team/")


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        print(uid)
        user = Team.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        #login(request, user)
        # return redirect('home')
        messages.success(request, 'Thank you for your email confirmation. Now you can login to your account.')
        #return HttpResponse('Please confirm your email address to complete the registration')
        return redirect(reverse_lazy('login'))
        #return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        messages.error(request, 'Activation link is invalid! Check your email again.')
        return redirect(reverse_lazy('login'))

'''
def team_profile(request, teamname, *args, **kwargs):
    view = TeamDetailView(**kwargs)
    view.pk = 3
    print(teamname)
    print(request)
    return view.get(request)
'''


'''
class CustomLoginView(LoginView):
    """
    Custom login view.
    """

    template_name = 'login.html'

    def post(self, request, *args, **kwargs):
        result = super(CustomLoginView, self).post(request, *args, **kwargs)
        print(result)
        return result
'''
