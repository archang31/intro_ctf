from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.

# Model Fields : https://docs.djangoproject.com/en/2.1/ref/models/fields/#django.db.models.Field

class Team(AbstractUser):
    # https://docs.djangoproject.com/en/2.1/topics/auth/default/#user-objects
    # username, password, email, first_name, and last_name

    #remember - blank effects the form validator. null is for the database. Do not do null for strings because
    # the default value is "" <- empty string and null not the same so will need to check for twoself.

    # required fields
    email_max_length = 150
    email_help_text = "Enter a valid email address (this address will be validated)."
    email = models.EmailField(max_length=email_max_length, null=False, blank=False,
            unique=True, help_text=email_help_text)
    points = models.IntegerField(default=0)
    solves = models.IntegerField(default=0)
    #optional fields (not in setup)
    motto = models.CharField(max_length=150, blank=True, default="", help_text="Please enter your team's motto (not required, 150 char max).")
    website = models.URLField(max_length=150, blank=True, default="", help_text="Please enter your team's website (not required, 150 char max).")
    description = models.TextField(max_length=1000, blank=True, default="", help_text="Please provide a short description of your team (1000 char max).")


    def __str__(self):
        return self.username
