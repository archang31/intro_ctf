from django.forms import CharField, EmailField
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
# https://github.com/django/django/blob/master/django/contrib/auth/validators.py
# from django.contrib.auth.validators import ASCIIUsernameValidator
from django.utils.translation import gettext_lazy as _
from django.db import models

from .models import Team

# General Django Application:
# https://docs.djangoproject.com/en/2.1/topics/auth/default/

# Specific User Object Information:
# https://docs.djangoproject.com/en/2.1/ref/contrib/auth/#django.contrib.auth.models.User

class TeamCreationForm(UserCreationForm):
    # validators are how you validate the data in input fields
    # https://docs.djangoproject.com/en/2.1/ref/forms/fields/#validators
    #username = CharField(validators=[ASCIIUsernameValidator])
    #email = EmailField(max_length=Team.email_max_length, label="Team email", help_text=Team.email_help_text)

    class Meta(UserCreationForm.Meta):
        model = Team
        # overriding default help_text for something shorter for tooltip
        fields = ('username', 'email', 'password1', 'password2')
        labels = {
            'username': _('Team name'),
            'email': _("Team email")
        }
        # for some reason, the password1 does not work. I had to add this directly
        # into the register.html template.
        help_texts = {
            'password1': _("At least 8 characters, and it can't be all numeric, a common password, or too similar to your team name"),
            'username': _("Your team's handle. Must be 150 characters or fewer. Letters, digits and @/./+/-/_ only."),
            'email': _('Enter a valid email address (just pick one for your team).  This address will be validated.'),

        }

    # https://docs.djangoproject.com/en/2.1/ref/forms/validation/#form-field-default-cleaning

class TeamChangeForm(UserChangeForm):

    class Meta:
        model = Team
        fields = ('motto', 'website', 'description', 'password')
